package main;

import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		/*Establecer un breakpoint en la primera instruccion y avanzar
		 * instruccion a instruccion (step into) analizando el contenido de las variables
		 */
		
		Scanner lector;
		int numeroLeido;
		int resultadoDivision;
		/* Encontramos el error en la linea 22 de c�digo cuando al realizar la division (la cual 
		 * da valores a la variable resuldadoDivision) nos salta la  excepcion aritmetica de java
		 * porque estamos intentando dividir el numero introducido por teclado entre 0, ya que es
		 * ahi donde finaliza el bucle descendente que da valores a la variable i (divisor en este caso).
		 * 
		 * 
		 * 
		 * Posible solucion;
		 * 
		 * En lugar de finalizar el bucle en i>=0 podemos no igualarlo a 0 y poner simplemente i>0.
		 * 
		 * Otra posible solucion seria en vez de igualarlo i>=0 hacerlo a 1. 
		 */
		lector = new Scanner(System.in);
		System.out.println("Introduce un numero");
		numeroLeido = lector.nextInt();
		
		for(int i = numeroLeido; i >= 0 ; i--){
			resultadoDivision = numeroLeido / i;
			System.out.println("el resultado de la division es: " + resultadoDivision);
		}
				
		lector.close();
	}

}
