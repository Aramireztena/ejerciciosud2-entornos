package main;

import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		/*
		 * Establecer un breakpoint en la primera instruccion y avanzar
		 * instruccion a instruccion (step into) analizando el contenido de las
		 * variables
		 * 
		 */

		Scanner input;
		int cantidadVocales, cantidadCifras;
		String cadenaLeida;
		char caracter;

		cantidadVocales = 0;
		cantidadCifras = 0;

		System.out.println("Introduce una cadena de texto");
		input = new Scanner(System.in);
		cadenaLeida = input.nextLine();
/* Encontramos el error en la l�nea 37 de c�digo, despu�s de introducir una cadena por teclado
*	el programa pasa a la siguiente instrucci�n en la que entra en un bucle, el cual se 
*	inicializa en 0, y se finaliza cuando llega a la cantidad de caracteres que tiene la cadena 
*		introducida por teclado. Sin embargo, a la hora de numerar una cadena siempre se empieza
*		dando el valor 0 al primer car�cter. Por lo tanto, el bucle al estar igualado a la longitud
*		de la cadena siempre se va a repetir una vez mas de lo que deber�a dando as� lugar a un error
*		(excepci�n) ya que en la posici�n 4 por ejemplo no devolver�a ning�n car�cter, puesto que no
*		hay. Es decir si introducimos como en el ejemplo una cadena de 4 caracteres, e igualamos la 
*		condici�n final del bucle a la longitud de la cadena (i<=4) como la primera posici�n es la
*		0 que corresponder�a con la letra �H� la �ltima que ser� el car�cter �A� ocupara la posici�n
*		3 (i=3) pero nuestro bucle no finaliza hasta que la i=4 y ah� es cuando no salta el error ya
*		que no existe car�cter en dicha posici�n que nos pueda devolver. 
*/
		for (int i = 0; i <= cadenaLeida.length(); i++) {
			caracter = cadenaLeida.charAt(i);
			if (caracter == 'a' || caracter == 'e' || caracter == 'i' || caracter == 'o' || caracter == 'u') {
				cantidadVocales++;
			}
			if (caracter >= '0' && caracter <= '9') {
				cantidadCifras++;
			}
		}
		System.out.println(cantidadVocales + " y " + cantidadCifras);
/*
 * Posibles Soluciones:
 * 
	En vez de igualar en la condici�n final del bucle con la longitud de la cadena dejar simplemente
	el sigo �<� as� el bucle finalizar�a una posici�n antes evitando el error. En lugar de terminar
	en i=4 (4=longitud cadena) lo har� en i=3.	
		
	Otra posible soluci�n seria dejar el bucle como esta e igualarlo a 
	cadenaLeida.length()-1. De esta forma evitar�amos el error porque
	el bucle se detendr�a en la posici�n 3.
 */
		input.close();
	}
}

